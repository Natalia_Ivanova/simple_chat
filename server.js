const express = require('express');
const http = require('http');
const mongoose = require('mongoose');
const path = require('path');
const { Server }= require('socket.io');
const Sockets = require("./services/socket");
const bodyParser = require('body-parser');

const app = express();
const server = http.createServer(app);
global.io = new Server(server);

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

global.io.on('connection', Sockets.connection);

server.listen(3000, () => {
    console.log('App listen port 3000')
})


