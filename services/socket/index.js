const formatMessage = require('../../helpers/messages');
const {userJoin, getCurrentUser, userLeave, getRoomUsers} = require('../../helpers/user')

const Sockets = {
    connection: (socket) => {

        socket.on('joinRoom', ({username, room}) => {
                const user = userJoin(socket.id, username, room);

                socket.join(user.room);

                socket.emit('message', formatMessage('ChatBot', 'Welcome to Chat!'));

                socket.broadcast
                    .to(user.room)
                    .emit('message', formatMessage('Chat Bot', `User ${user.username} hes joined to chat`));

                global.io.to(user.room).emit('roomUsers', {
                    room: user.room,
                    users: getRoomUsers(user.room)
                });
            }
        )

        socket.on('disconnect', () => {
            const user = userLeave(socket.id);

            if (user) {
                global.io.to(user.room).emit(
                    'message',
                    formatMessage('ChatBot', `${user.username} has left the chat`)
                )
                global.io.to(user.room).emit('roomUsers', {
                    room: user.room,
                    users: getRoomUsers(user.room)
                });
            }
        });

        socket.on('chatMessage', (msg) => {
            const user = getCurrentUser(socket.id);

            global.io.to(user.room).emit('message', formatMessage(`${user.username}`, msg))
        })
    }
}

module.exports = Sockets
